Matrix elements: Radial code
******************************

There are 2 main catecories of basis functions, memory intensive plane wave and real space grids, and localized basis sets, which resemble atomic orbitals. 
Note, that technically real space grid is not a basis, since it is based on finite difference approximation of the kinetic energy operator, i.e. one approximates the matrix element, where as in the other cases, the approximation
was restricted only by the finite amount of basis functions, but the matrix elements were exact.

There are several ways to obtain matrix elements for localized basis sets. 
Matrix elements with Gaussian basis sets are fast to evaluate rapidly (but full implementation is rather tedious). 
Fourier-Bessel method may be used when evaluating numerical radial basis.
These are technical methods, aimed for computationally engineered balance between efficiency and accuracy.

In this tutorial, since we wish to calculate matrix elements only for demonstrative purposes, we bybass the technical difficulties by introducing a method, which is technically straightforward and numerically robust, but perhaps performance might be slightly 
compromised: We will use the Becke integration method for all matrix elements.

Radial integration
==================

To get a feeling of calculating matrix elements, we will solve hydrogen atom wave functions with localized basis set. We require i) overlap matrix elements ii) kinetic energy matrix elements iii) potential energy matrix elements.

For integrals with radial symmetry, we may switch to spherical coordinates, and obtain

::math
    
    \int d{\textbf r} f({\textbf r}) = int dr f(r) Y_{00}(\hat r) = 4 pi \int dr r^2 f(r).

The first step is to choose an integration quadrature. Here we use the simplest possible, equispaced quadrature.

insert file radialintegrals.py

alpha num   exp.  fract.
1.000 5.568 5.568 1.000000
2.000 1.969 1.969 1.000000
3.000 1.072 1.072 1.000000
4.000 0.696 0.696 1.000000
5.000 0.498 0.498 1.000000
6.000 0.379 0.379 1.000000
7.000 0.301 0.301 1.000000
8.000 0.246 0.246 1.000000
9.000 0.206 0.206 1.000000


Overlap matrix elements
=======================

We choose a radial basis with
