import numpy as np
def integrate(f, r, dV):
    return np.sum(f(r)*dV)

# Test case: Analytically
# int dr exp(-alpha r^2) = sqrt(2*pi/alpha)

N = 1000 # Number of radial grid points
h = 0.01 # Grid spacing
r = (np.array(range(0, N))+0.5)*h
dV = 4*np.pi*r**2*h
print("%-5s %-5s %-5s %-6s" % ("alpha", "num", "exp.", "fract."))
for alpha in np.arange(1, 10, 1):
    numerical_integral = integrate(lambda r : np.exp(-alpha*r**2), r, dV)
    analytical_integral = (np.pi/alpha)**(3/2)
    print("%.3f %.3f %.3f %.6f" % (alpha, numerical_integral, analytical_integral, numerical_integral/analytical_integral))
    assert(abs(numerical_integral-analytical_integral)<1e-5)
